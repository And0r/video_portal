class Tag < ActiveRecord::Base
    has_many :videos_tags
    has_many :videos, :through => :videos_tags
end
