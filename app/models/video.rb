class Video < ActiveRecord::Base
    has_many :videos_tags
    has_many :tags, :through => :videos_tags
end
