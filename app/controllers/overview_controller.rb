class OverviewController < ApplicationController
    def index
        tags = params[:filter]

        if tags
            @videos = Video.joins(:tags).where({"tags.id" => [tags]})
        else
            @videos = Video.joins('LEFT OUTER JOIN videos_tags ON videos.id = videos_tags.video_id')
        end

        @filter = Tag.all
    end

    def _video
        index
        render :layout => false
    end
end
