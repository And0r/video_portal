class EditController < ApplicationController
    before_action :only_admin

    def index
        video_id = params[:id]

        if video_id
            @video = Video.find(video_id)
        else
            @video = Video.new
        end
        @tags = Tag.all
    end

    def post
        video_id = params[:id]
        if video_id.present?
            video = Video.find_or_create_by({:id => video_id})
        else
            video = Video.new
        end
        video.name = params[:name]
        video.description = params[:description]
        video.youtoobe_id = params[:youtoobe_id]
        video.save

        # Save activ Tags
        VideosTag.where(:video_id => video_id).destroy_all
        if params[:tag]
            puts "alle tags: "+params[:tag].inspect
            params[:tag].each do |tag|
                vt = VideosTag.new
                vt.video_id = video_id
                vt.tag_id = tag
                vt.save
            end
        end

        redirect_to action: "index", id: video.id
    end
end
