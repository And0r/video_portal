class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_permissions

    
  private
  
  def set_permissions
    if params[:admin_hack] == AccesKey.first.key
        session[:admin] = 1
    end
    @admin = session[:admin]
  end

  def only_admin
    redirect_to "", alert: 'Sorry du hast keine Berechtigung...' if not session[:admin] == 1
  end
end
