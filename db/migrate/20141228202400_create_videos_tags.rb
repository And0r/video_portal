class CreateVideosTags < ActiveRecord::Migration
  def change
    create_table :videos_tags do |t|
      t.integer :video_id, null: false
      t.integer :tag_id, null: false

      t.timestamps null: false
    end
  end
end
