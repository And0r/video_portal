class CreateAccesKeys < ActiveRecord::Migration
  def change
    create_table :acces_keys do |t|
      t.string :key

      t.timestamps null: false
    end
  end
end
