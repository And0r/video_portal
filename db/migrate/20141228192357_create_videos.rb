class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|

      t.timestamps null: false
      t.string :name
      t.text :description
      t.string :youtoobe_id, null: false 
    end
  end
end
